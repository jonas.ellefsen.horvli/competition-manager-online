﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Competitor_Manager_Online.Models;
using Microsoft.Extensions.Configuration;
using Competition_Manager;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace Competitor_Manager_Online.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly EFCoreWebContext _dbContext;

        public HomeController(ILogger<HomeController> logger, EFCoreWebContext context)
        {
            _logger = logger;
            _dbContext = context;
        }

        public IActionResult Index()
        {
            ICollection<Competitor> competitors = _dbContext.Competitors.Include(c => c.Team).ToList();
            return View(competitors);
        }

        public IActionResult Competitor(int? id)
        {
            Competitor comp = _dbContext.Competitors.Find(id);
            ViewBag.Teams = new SelectList(_dbContext.Teams.ToList(), "ID", "Name");
            return View(comp);
        }

        public IActionResult CreateCompetitor()
        {
            ViewBag.Teams = new SelectList(_dbContext.Teams.ToList(), "ID", "Name");
            return View();
        }

        public IActionResult PutCompetitor(Competitor competitor)
        {
            _dbContext.Competitors.Update(competitor);
            _dbContext.SaveChanges();
            return RedirectToAction(nameof(Index));
        }

        public IActionResult PostCompetitor(Competitor competitor)
        {
            _dbContext.Competitors.Add(competitor);
            _dbContext.SaveChanges();
            return RedirectToAction(nameof(Competitor), new { id = competitor.ID });
        }

        public IActionResult DeleteCompetitor(int? id)
        {
            Competitor competitor = _dbContext.Competitors.Find(id);
            _dbContext.Competitors.Remove(competitor);
            _dbContext.SaveChanges();
            return RedirectToAction("Index");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}

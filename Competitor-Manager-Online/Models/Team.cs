﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Competition_Manager
{
    public class Team
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int Division { get; set; }

        public ICollection<Competitor> Members { get; set; }

        public Team() { }
    }
}

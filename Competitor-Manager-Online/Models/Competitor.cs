﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Competition_Manager
{
    public class Competitor
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public int Weight { get; set; }
        public int Height { get; set; }
        public Coach Coach { get; set; }
        public int? TeamId { get; set; }
        public Team Team { get; set; }
        public ICollection<CompetitorSkills> competitorSkills { get; set; }


        public Competitor() { }

        public override string ToString()
        {
            string competitorText = $"Id: {ID}\tName: {Name}\tAge: {Age}\tWeight: {Weight}kg\tHeight: {Height}cm";
            if (competitorSkills != null && competitorSkills.Count > 0) {
                competitorText += "\nSkills:";
                foreach (CompetitorSkills cs in competitorSkills)
                {
                    competitorText += $"\n{cs.Skill}";
                }
            }
            return competitorText;
        }
    }
}

# Competition Manager Online

Competition Manager Online is a continuation of the Competitor Manager project.
Making it into a MVC Web application where user can create, read, update and delete competitors.

## Components

### Competitor
Competitor represents a person and has one or more relations to any of the other components.
A competitor may be in 1 team, have 1 coach, and can have none or many skills through CompetitorSkills.

### Coach
Is the trainer and coach of one competitor.

### Team
A team that can consist of many Competitors.

### Skill
A skill competitors may have through CompetitorSkills.

### CompetitorSkills
CompetitorSkills exists to maintain a many to many relation between Competitors and Skills.